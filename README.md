# pi-coffeecam #

A Raspberry Pi webcamera which can be accessed via browser

### Prerequisites ###

* Rapsberry Pi Model 2 or Raspberry Pi Zero W - Untested on other versions
* Camera module 2
* micro sdcard - for OS and apache installation, and storage of our scripts
* OS: Raspbian 8 (jessie) - Untested on other versions
* These instructions assume you have installed vim on your pi, but any other text editor will work as well

## Overview ##
The Pi will run an apache webserver which serves the latest photo and automatically refreshes the image tag periodically. The camera will store (and overwrite) the latest photo on a [ramdisk](https://en.wikipedia.org/wiki/RAM_drive) to prevent sdcard deterioration. A symlink is used to link the latest photo to our webserver.

### How do I get set up? ###

Open a terminal window or connect to your Pi via SSH

#### Upgrade the pi ####
Make sure your pi is fully up to date by running
```sh
sudo apt-get update
sudo apt-get dist-upgrade
```

#### Create Ram disk service ####
* Create a file called "init_ramdisk.sh" in your home folder, and paste the following code into it and save:
```sh
#!/bin/bash

mount -osize=10m tmpfs /ramdisk -t tmpfs
```

* Navigate to /etc/systemd/system 
* create a file called "init-ramdisk.service, and paste the following code into it and save:
```sh
[Unit]
After=network.target

[Service]
ExecStart=/home/pi/init_ramdisk.sh

[Install]
WantedBy=multi-user.target
```


#### Create Camera service ####
Create a file called "webcam.sh" in your home folder (standard folder is /home/pi). Paste the following code into it and save:
```sh
#!/bin/bash

raspistill --nopreview --timeout 0 -tl 1000 -w 640 -h 480 -q 20 -o /ramdisk/latest.jpg
```

* Navigate to /etc/systemd/system 
* create a file called "webcam.service, and paste the following code into it and save:
```sh
[Unit]
Description=Coffeecam service
After=init-ramdisk.service

[Service]
ExecStart=/home/pi/webcam.sh
WorkingDirectory=/home/pi/
Restart=always
User=pi

[Install]
WantedBy=multi-user.target
```

#### Install Apache and configure webpage ####


Install apache:
```sh
 sudo apt-get install apache2 -y
```

Create a folder for your new webpage:
```sh
 cd /var/www/html
 sudo mkdir webcam
```

Create the webcam webpage:
```sh
cd /var/www/html/webcam
vim index.html
```

Copy the html code from "index.html" and save it in /var/www/html/webcam:

Create a symlink for the ramdisk
```sh
 cd /var/www/html/webcam/
 ln -s /ramdisk/latest.jpg
```



#### Reboot the pi ####
```sh
 sudo reboot
```

Your webcam webpage should now be accessible on http://[your raspberrypi ip address]/webcam